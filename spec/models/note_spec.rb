require 'rails_helper'

RSpec.describe Note, type: :model do
  it "raises exception if content has fewer words than title" do
    expect { 
      @note = Note.new
      @note.title = 'foo bar baz'
      @note.content = 'foo bar'
      @note.save! 
    }.to raise_error ActiveRecord::RecordInvalid
  end

  it 'raises exception if title is blank' do
    expect { 
      @note = Note.new
      @note.content = 'bar baz'
      @note.save!
    }.to raise_error ActiveRecord::RecordInvalid
  end

end
