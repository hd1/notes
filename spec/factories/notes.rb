FactoryGirl.define do
  factory :note do
    title 'lorem'
    contents 'lorem ipsum'
  end

  factory :note1 do
    title ''
    contents 'foo'
  end

  factory :note2 do
    title 'foo'
    contents ''
  end
end
