## README

This README documents what is necessary to get the
application up and running.

Things you may want to cover:

*   Configuration

   `bundle install` installs all dependencies.

*   Database creation

   `rake db:create db:migrate` handles the database creation/initialization, do not set RAILS_ENV, production is live and test is in-memory.

*   How to run the test suite

   `bundle exec rspec` handles this -- note deployment to heroku isn't possible if tests do not pass.

*   Services (job queues, cache servers, search engines, etc.)

   None yet

*   Deployment instructions

   Have Hasan do a git push
