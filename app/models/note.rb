class Note < ActiveRecord::Base
  validates :title, presence: true
  validate :content_should_have_more_words_than_title
  belongs_to :user
  def content_should_have_more_words_than_title
    unless title.to_s.split.size < content.to_s.split.size 
      errors.add(:content, 'Number of words in content should be more than the number of words in title')
    end
  end
end

