json.array!(@notes) do |note|
  json.extract! note, :id, :content, :title, :user
  json.url note_url(note, format: :json)
end
