require 'odyssey'

module NotesHelper
  def admin?
    current_user.id == 1
  end

  def score(content)
    Rails.logger.debug("#{content} should now be plaintext")
    Odyssey.flesch_kincaid_re(content, false)
  end
end
