class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  # GET /notes
  # GET /notes.json
  def index
		@users = User.order(:created_at)
		if current_user.equal? @users[0] 
			@notes = Note.connection.select_all('SELECT n.id, n.title, n.content, u.email from notes n join users u on u.id = n.user_id ORDER BY n.created_at').rows
		else
      @notes = Note.all
		end
    respond_to do |format|
    	format.html { }
	    format.xml {
		    @users = User.order(:created_at)
		    if @users.first == current_user then
			  render xml: @notes
		    else
			    return head(:unauthorized)
		    end
	    }
        format.json { }
    end
  end

  # GET /notes/1
  # GET /notes/1.json
  def show
    @note = Note.find(params[:id])
    respond_to do |format|
      format.html { }
      # from http://stackoverflow.com/a/7293155/783412
      format.pdf {
	      pdf = Prawn::Document.new
        pdf.text @note.content
        send_data pdf.render, :filename => "#{@note.hash}.pdf", :type => "application/pdf", :disposition => 'inline' 
      }
    end
  end

  # GET /notes/new
  def new
    @note = Note.new
  end

  # GET /notes/1/edit
  def edit
  end

  # POST /notes
  # POST /notes.json
  def create
    @note = Note.new(note_params)
    @note[:user_id] = current_user.id

    respond_to do |format|
      if @note.save
        format.html { redirect_to @note, notice: 'Note was successfully created.' }
        format.json { render :show, status: :created, location: @note }
      else
        format.html { render :new }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notes/1
  # PATCH/PUT /notes/1.json
  def update
    respond_to do |format|
      if @note.update(note_params)
        format.html { redirect_to @note, notice: 'Note was successfully updated.' }
        format.json { render :show, status: :ok, location: @note }
      else
        format.html { render :edit }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notes/1
  # DELETE /notes/1.json
  def destroy
    @note.destroy
    respond_to do |format|
      format.html { redirect_to notes_url, notice: 'Note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_note
      @note = Note.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def note_params
      params.require(:note).permit(:content, :title, :user)
    end
end
