class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def disqus
    @user = User.from_omniauth(request.env["omniauth.auth"])
    if @user.persisted?
      set_flash_message(:notice, :success, :kind => "Disqus") if is_navigational_format?
    else
      @user.email = "#{request.env['omniauth.auth']['uid']}-#{request.env['omniauth.auth']['provider']}@mailinator.com"
      session["devise.disqus_data"] = request.env["omniauth.auth"].except('extra')
    end
      sign_in(:user, @user)
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
  end

  def twitter
    @user = User.from_omniauth(request.env['omniauth.auth'])
    if @user.persisted?
      sign_in(:user, @user)
    else
      @user.email = "#{request.env['omniauth.auth']['uid']}-#{request.env['omniauth.auth']['provider']}@mailinator.com"
      session["devise.twitter_data"] = request.env["omniauth.auth"].except('extra')
    end
      sign_in(:user, @user)
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
  end

  def google_oauth2
    @user = User.from_omniauth(request.env['omniauth.auth'])
    if @user.persisted?
      set_flash_message(:notice, :success, :kind => 'Google')
    else
      @user.email = "#{request.env['omniauth.auth']['uid']}-#{request.env['omniauth.auth']['provider']}@mailinator.com"
      session["devise.google_data"] = request.env["omniauth.auth"].except('extra')
    end
      sign_in(:user, @user)
      sign_in_and_redirect @user, :event => :authentication
  end	
end
