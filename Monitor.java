import java.awt.HeadlessException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import edu.stanford.ejalbert.BrowserLauncher;
import edu.stanford.ejalbert.exception.BrowserLaunchingInitializingException;
import edu.stanford.ejalbert.exception.UnsupportedOperatingSystemException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.yaml.snakeyaml.Yaml;

public class Monitor {
	final JFrame frame = new JFrame();
	
	public Monitor() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static Vector<String> getColumnIdentifiers() {
		Vector<String> columnIds = new Vector<String>();
		columnIds.add("Title");
		columnIds.add("Author");
		columnIds.add("Date");
		return columnIds;
	}

	public void title() {
		String now = format_date(new DateTime());
		String jdbc_url = System.getProperty("jdbc.url");
		this.frame.setTitle("Monitoring "+jdbc_url+" at "+now+", press 'r' to refresh");
	}
	
	public void run() {
		String jdbc_url = System.getProperty("jdbc.url");
		
		try {
			Class.forName("org.postgresql.Driver").newInstance();
		} catch (Exception e) {
			System.err.println(new DateTime()+": "+e.getMessage());
			System.exit(-1);
		}
		Vector<Vector<String>> data = RetrieveData();

		try {
			
			DefaultTableModel model = new DefaultTableModel(data, Monitor.getColumnIdentifiers());
			final JTable table = new JTable(model);
			table.addMouseListener(new MouseAdapter() {
					public void mousePressed(MouseEvent evt) {
						int currentRow = table.rowAtPoint(evt.getPoint());
						if (evt.getClickCount() == 2) {
							open(RetrieveData().get(currentRow).get(3));
						}
					}
					// FIXME: does not work
					public void open(String id) {
						String location = id;
						System.err.println(new DateTime()+": "+location);
						try {
							BrowserLauncher launcher = new BrowserLauncher();
							launcher.openURLinBrowser(location);
						} catch (BrowserLaunchingInitializingException e) {
							System.err.println(new DateTime()+": "+e.getMessage());
						} catch (UnsupportedOperatingSystemException e) {
							System.err.println(new DateTime()+": "+e.getMessage());
						}
					}
				});
			table.addKeyListener(new KeyAdapter() {
					public void keyPressed(KeyEvent evt) {
						if (evt.getKeyChar() == 'r') {
							System.err.println(new DateTime()+": Refresh triggered");
							Vector<Vector<String>> data = Monitor.RetrieveData();
							DefaultTableModel model = (DefaultTableModel)table.getModel();
							model.setDataVector(data, Monitor.getColumnIdentifiers());
							model.fireTableDataChanged();
						}
					}
				});
			JScrollPane pane = new JScrollPane(table);
			this.title();
			this.frame.add(pane);
			this.frame.pack();
			this.frame.setVisible(true);
		} catch (HeadlessException e) {
      
			System.err.println(System.err.format("%s\t%s\t%25s\t%29s%n", "Title", "Author", "Date", "Link"));
			for (Vector<String> row : data) {
				System.err.println(String.format("%s\t%s\t%25s\t%29s%n", row.get(1), row.get(2), row.get(3), "http://notes.d8u.us/notes/"+row.get(4)));
			}
		}
	}

	public static String format_date(DateTime date) {
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		return fmt.print(date);
	}
	private static Vector<Vector<String>> RetrieveData() {
		Vector<Vector<String>> data = new Vector<Vector<String>>();
		ResultSet results = null;
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(System.getProperty("jdbc.url"));
			Statement statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			results = statement.executeQuery("select n.title as title, n.content as content, u.email as user, n.created_at as created_at, n.id as id from notes n join users u on u.id = n.user_id order by n.created_at");
			while (results.next()) {
				Vector<String> row = new Vector<String>();
				row.add(results.getString(1));
				row.add(results.getString(3));
				row.add(format_date(new DateTime(results.getTimestamp(4))));
				row.add("http://notes.d8u.us/notes/"+results.getString(5));
				data.add(row);
			}
		} catch (SQLException e2) {
		  	System.err.println(new DateTime()+": "+e2.getMessage());
		  	System.exit(-1);
		} finally {
			if (conn != null) {
				try { 
					conn.close();
				} catch (Throwable e3) {
					System.err.println(new DateTime()+": "+e3.getMessage());
				}
			}
		}
		
		return data;
	}
	public static void main(String[] args) {
		try {
			ArrayList<String> key = new ArrayList<String>();
			if (System.getProperty("jdbc.url") == null) {
				try {
					Yaml yaml = new Yaml();
					FileInputStream input = new FileInputStream("config/database.yml");
					Map<String,Object> yamlContents = (Map<String,Object>) yaml.load(input);
					System.err.println(new DateTime()+": "+((Map<String,Object>)(yamlContents.get("production"))).keySet());
					Map<String,Object> yamlKeys = (Map<String,Object>)yamlContents.get("production");
					String jdbcUrl = "jdbc:"+yamlKeys.get("adapter")+"://"+yamlKeys.get("host")+"/"+yamlKeys.get("database")+"?user="+yamlKeys.get("user")+"&password="+yamlKeys.get("password")+"&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
					System.err.println(new DateTime()+": jdbc url is "+jdbcUrl);
					System.setProperty("jdbc.url", jdbcUrl);
					input.close();
				} catch (IOException e) {
					System.err.println(new DateTime()+": "+e.getMessage());
				}
			}
			Monitor monitor = new Monitor();
			monitor.run();
		} catch (HeadlessException e) {
			Vector<Vector<String>> data = RetrieveData();
			System.err.println(String.format("%s\t%s\t%25s\t%29s%n", "Title","Author","Date","Link"));
			for (Vector<String> row : data) {
				System.err.println(String.format("%s\t%s\t%25s\t%29s%n", row.get(0), row.get(1), row.get(2), row.get(3)));
			}
		}
	}
}
