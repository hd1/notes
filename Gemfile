source 'https://rubygems.org'
ruby '2.2.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.3'
# for jwt, need to do this
gem 'rack-cors', :require => 'rack/cors'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
#gem 'therubyracer', platforms: :ruby
gem 'node'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem 'bootstrap-sass', '~> 3.3.5'

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

gem 'awesome_nested_set'  # or same gem
gem 'devise', '>= 2.0.0'
gem 'doorkeeper'
gem 'haml'                # or gem 'slim'
gem 'htmlcompressor'
gem 'jwt', '1.5.1', :git => 'https://github.com/progrium/ruby-jwt.git'
gem 'odyssey'
gem 'omniauth', '~> 1.0'
gem 'omniauth-disqus'
gem "omniauth-google-oauth2"
gem 'omniauth-twitter'
gem 'prawn'
gem 'proof-rails'
gem 'pry-rails'
gem 'redcarpet'
gem 'yui-compressor'

group :development, :test do
  # call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'sqlite3'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'heroku'

  # RSpec
  gem 'rspec-rails'  

  gem 'dotenv-rails'
  gem 'factory_girl_rails'  
end

group :production do  
  gem 'pg'
  gem 'puma'
  gem 'rails_12factor'  
end

group :test do  
  gem 'ci_reporter'
  gem 'capybara'  
  gem 'database_cleaner'  
  gem 'faker'  
  gem 'launchy'  
  gem 'memory_test_fix'
  gem 'mocha'
  gem 'poltergeist'  
  gem 'selenium-webdriver'  
  gem 'shoulda'
  gem 'simplecov'
end
